package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/art.frela/chat/infra"
)

var (
	build    string
	githash  string
	filename = "main.go"
	varName  = "version"
	version  = "0.0.3"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	quit := make(chan os.Signal)

	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	serv := infra.NewServer(ctx, version, build, githash)
	serv.Run()
	<-quit
	cancel()
	serv.Stop()
}
