############################
# STEP 1 build executable binary
############################
FROM golang:buster AS builder
LABEL maintainer="art.frela@gmail.com" version="0.0.1"
ARG CI_JOB_ID
ARG CI_COMMIT_SHA
ENV GITLAB_BUILD_NUMBER ${CI_JOB_ID}
ENV CI_COMMIT_SHA ${CI_COMMIT_SHA}
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ADD ./go.* /go/src/chat/
ADD ./*.go /go/src/chat/
ADD ./domain/*.go /go/src/chat/domain/
ADD ./usecases/*.go /go/src/chat/usecases/
ADD ./infra/*.go /go/src/chat/infra/
ADD ./config.yaml /go/src/chat/
ADD ./assets/* /go/src/chat/assets/
WORKDIR /go/src/chat/
RUN CGO_ENABLED=0 go build -ldflags="-X 'main.build=$GITLAB_BUILD_NUMBER' -X 'main.githash=$CI_COMMIT_SHA'"
############################
# STEP 2 build a small image
############################
FROM alpine
RUN apk add --no-cache tzdata wget
ENV CHAT_HTTPD_PORT=8000
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY --from=builder /go/src/chat/chat /go/bin/chat/chat
COPY --from=builder /go/src/chat/config.yaml /go/bin/chat/config.yaml
COPY --from=builder /go/src/chat/assets/* /go/bin/chat/assets/
WORKDIR /go/bin/chat/
EXPOSE 8000
CMD ["./chat"]